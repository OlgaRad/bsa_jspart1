const API_URL = 'https://api.github.com/';
const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');
const fightersDetailsMap = new Map();

function callApi(endpoind, method) {
  const url = API_URL + endpoind
  const options = {
    method
  };

  return fetch(url, options)
    .then(response => 
      response.ok 
        ? response.json() 
        : Promise.reject(Error('Failed to load'))
    )
    
    .catch(error => { throw error });
}

// const API_URLL = 'https://api.github.com/repos/sahanr/street-fighter/contents/fighters.json?ref=master';

// fetch(API_URLL)
//   .then(response => response.json())
//   .then(file => {
//     const fighters = JSON.parse(atob(file.content));
//     console.log(fighters);
//   });

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'repos/sahanr/street-fighter/contents/fighters.json';
      
      const apiResult = await callApi(endpoint, 'GET');
      
      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
  	try {
  	const endpoint = 'repos/sahanr/street-fighter/contents/fighter-${_id}.json';
  	 const apiRes = await callApi(endpoint, 'GET');
  	 
  	 return JSON.parse(atob(apiResult.content));
  	 } catch (error) {
  	 	throw error;
  	 }
  }
}


const fighterService = new FighterService();

class View {
  element;

  createElement({ tagName, className = '', attributes = {} }) {
    const element = document.createElement(tagName);
    element.classList.add(className);
    
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
  }
}

class FighterView extends View {
  constructor(fighter, handleClick) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }
}

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  handleFighterClick(event, fighter) {
    this.fightersDetailsMap.set(fighter._id, fighter);
    console.log('clicked')
    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal
  }
}

class Fighter {
	constructor(name, health, attack, defense) {
		this.name = name;
		this.health = health;
		this.attack = attack;
		this.defense = defense;
		this.getHitPower(attack);
		this.getBlockPower(defense);
	}
	getHitPower(attack) {
		let min = 1;
		let max = 2;
		function ratio(min, max) {
			let randNumber = Math.random()*(max-min+1)+min
			return randNumber;
		}
		const criticalHitChance = ratio(min,max);
		const powerHit = attack*criticalHitChance;
		return powerHit;
	}
	
	getBlockPower(defense) {
		let min = 1;
		let max = 2;
		function ratio(min, max) {
			return Math.random()*(max-min+1)+min;
		}
		const dodgeChance = ratio(min,max);
		const powerBlock = defense*dodgeChance;
		return powerBlock;
	}
}

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;
      const fighter = new Fighter('Rye', 'good', 15, 20);
      console.log(fighter.getHitPower(15));
      console.log(fighter.getBlockPower(20));


      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

new App();